# Django-Select2

**Date:** 2012-12-23

----

Although [Django-Select2](https://github.com/applegrew/django-select2) works
as advertised, there's one thing that makes it hard to bend to your will:
`script` tags are included in the widget HTML.

Now, that's a fine approach to get everything up and running, but it means I
have to include jQuery in the `head` element of the HTML, while I prefer to
include it at the bottom. And it means you can't include a form like that
inside a `script` element for client-side templating purposes because
`script` elements inside `script` elements means mayhem.

Besides, Select2 has [issues on mobile devices](https://github.com/ivaynberg/select2/issues/466).

I've previously worked with [Django-Selectable](https://bitbucket.org/mlavin/django-selectable)
and I'm going back to it. Django-Selectable *does* support dynamically added
forms and besides some styling issues (more to do with jQuery UI's complicated
HTML and CSS) it seems to be the superior option if you don't absolutely have
to use Select2.