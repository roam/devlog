# Java, truststores and cacerts

**Date:** 2013-01-03

----

Imagine my surprise when a piece of code for replying to an e-mail message that worked fine a few months ago, suddenly didn't work. `SSLHandshakeException` on the left, `PKIX path building failed`on the right. Simply by trying to send an e-mail through Gmail, and if they were to be having SSL certificate problems, I'd have been reading about it for a few days.

The actual culprit: we've since added a custom truststore to workaround a limitation of one of the backend services we use. And when defining a custom truststore, Java apparently thinks the right thing to do is to ignore the default `cacerts`. Which means ignoring every single valid SSL certificate in the world. *Joy!*

Fortunately, this is easy to solve by either trying to define multiple truststore managers or… by importing the `cacerts` into your custom truststore like this:

    keytool -importkeystore \
    	-srckeystore <path>/lib/security/cacerts \
    	-destkeystore <yourtruststore.ts> \
    	-srcstorepass changeit \
    	-deststorepass <yourtspassword>

